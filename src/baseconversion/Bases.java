package baseconversion;

import java.util.Random;

/**
 * The <code>Bases</code> class handles data for converting to and from
 *  specified bases, as well as randomly selecting values in a range.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Aug 31, 2016
 *
 */
public class Bases {
    // CONSTANTS
    private final String key = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    // ATTRIBUTES
    private int startBase, endBase, range;
    private String startValue, endValue;
    
    // CONSTRUCTORS
    public Bases(int startBase, int endBase, int range) {        
        this.startBase  = startBase;
        this.endBase    = endBase;
        this.range      = range;
        
        generateRandomValue();
    }
    
    // ACCESSORS
    public int getStartBase() { return startBase; }
    public int getEndBase() { return endBase; }
    public String getStartValue() { return startValue; }
    public String getEndValue() { return endValue; }
    
    // MUTATORS
    public void setStartBase(int startBase) { this.startBase = startBase; }
    public void setEndBase(int endBase) { this.endBase = endBase; }
    public void setRange(int range) { this.range = range; }
    
    // METHODS
    
    public String baseToBase(String value) {
        return fromBase10(endBase, toBase10(startBase, value));
    }
    
    public void generateRandomValue() {
        Random rand = new Random();
        int value10 = rand.nextInt(range - 1) + 1;

        startValue  = fromBase10(startBase, value10);
        endValue    = fromBase10(endBase, value10);
    }
    
    public boolean isValidBase(int base) {
        return base >= 2 && base <= key.length();
    }
    
    // PRIVATE HELPERS
    
    private int toBase10(int base, String value) {
        int output = 0, power = 0;
        
        while(!value.isEmpty()) {
            output += (key.indexOf(value.substring(value.length() - 1)) * (int)Math.pow(base, power++));
            value = value.substring(0, value.length() - 1);
        }
        
        return output;
    }
    
    private String fromBase10(int base, int value) {
        String output = "";
        
        while(value > 0) {
            output = key.charAt(value % base) + output;
            value /= base;
        }
        
        return output;
    }
}
