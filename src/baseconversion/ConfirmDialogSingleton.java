package baseconversion;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * The <code>ConfirmDialogSingleton</code> class is a singleton that displays a
 *  message dialog that contains a single close button.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Aug 31, 2016
 *
 */
public class ConfirmDialogSingleton extends Stage{
    // STATIC SINGLETON
    static ConfirmDialogSingleton singleton = null;
    
    // ATTRIBUTES
    Label messageLabel;
    Button closeButton;
    
    VBox mainVBox;
    Scene scene;
    
    // CONSTRUCTORS
    private ConfirmDialogSingleton() {}
    
    public static ConfirmDialogSingleton getSingleton() {
        if(singleton == null) { singleton = new ConfirmDialogSingleton(); }
        return singleton;
    }
    
    // METHODS
    public void init(Stage owner) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        
        messageLabel = new Label();
        messageLabel.setWrapText(true);
        
        closeButton = new Button("Close");
        closeButton.setOnAction(e -> { ConfirmDialogSingleton.this.close(); });
        
        mainVBox = new VBox(messageLabel, closeButton);
        mainVBox.setAlignment(Pos.CENTER);
        mainVBox.setPadding(new Insets(20));
        mainVBox.setSpacing(20);
        
        scene = new Scene(mainVBox, 280, 90);
        setScene(scene);
        
        scene.setOnKeyPressed(e -> {
            if(e.getCode() == KeyCode.ENTER) {
                ConfirmDialogSingleton.this.close();
            }
        });
    }
    
    public void show(String title, String message) {
        setTitle(title);
        messageLabel.setText(message);
        
        showAndWait();
    }
}
