package baseconversion;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * The <code>MainApp</code> class launches the base conversion program and
 *  builds the GUI and various handlers.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Aug 31, 2016
 *
 */
public class MainApp extends Application {
    // CONSTANTS
    final int
            WINDOW_WIDTH    = 300,
            WINDOW_HEIGHT   = 310,
            TEXTFIELD_WIDTH = 150;
    
    // ATTRIBUTES
    Label baseFromLabel, baseToLabel, rangeLabel, promptLabel, statusLabel;
    TextField baseFromField, baseToField, rangeField, valueField, answerField;
    Button generateButton, checkButton, answerButton, quitButton;
    
    VBox mainVBox;
    Scene scene;
    
    ConfirmDialogSingleton dialog;
    
    Bases bases;
    
    // LAUNCH JAVAFX APPLICATION
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // INITIALIZE SINGLETONS
        dialog = ConfirmDialogSingleton.getSingleton();
        dialog.init(primaryStage);
        
        // INITIALIZE DATA
        bases = new Bases(10, 2, 10000);
        
        // SETUP THE GUI
        buildGUI(primaryStage);
        setHandlers();
        setListeners();
    }
    
    // BUILD JAVAFX GUI
    public void buildGUI(Stage primaryStage) {
        baseFromLabel   = new Label("Initial Base:");
        baseToLabel     = new Label("Ending Base:");
        rangeLabel      = new Label("Range (decimal):");
        
        baseFromField   = buildTextField("2", TEXTFIELD_WIDTH);
        baseToField     = buildTextField("16", TEXTFIELD_WIDTH);
        rangeField      = buildTextField("10000", TEXTFIELD_WIDTH);
        
        promptLabel = new Label();
        statusLabel = new Label();
        
        promptLabel.setWrapText(true);
        promptLabel.setMinHeight(40);
        
        valueField = buildTextField("", TEXTFIELD_WIDTH);
        valueField.setEditable(false);
        valueField.setDisable(true);
        
        answerField = buildTextField("", TEXTFIELD_WIDTH);
        answerField.setDisable(true);
        
        generateButton  = new Button("Generate");
        quitButton      = new Button("Quit");
        answerButton    = new Button("Solution");
        checkButton     = new Button("Check");
        
        answerButton.setDisable(true);
        checkButton.setDisable(true);
        
        mainVBox = new VBox(alignedHBox(baseFromLabel, baseFromField),
                            alignedHBox(baseToLabel, baseToField),
                            alignedHBox(rangeLabel, rangeField),
                            alignedHBox(new Label(""), generateButton),
                            alignedHBox(promptLabel, valueField),
                            alignedHBox(statusLabel, answerField),
                            alignedHBox(quitButton, answerButton, checkButton));
        mainVBox.setPadding(new Insets(10));
        mainVBox.setSpacing(10);
        
        scene = new Scene(mainVBox, WINDOW_WIDTH, WINDOW_HEIGHT);

        // SET INITIAL PROPERTIES OF THE APPLIATION
        primaryStage.setTitle("Bases");
        primaryStage.setWidth(WINDOW_WIDTH);
        primaryStage.setHeight(WINDOW_HEIGHT);
        primaryStage.setMinWidth(WINDOW_WIDTH);
        primaryStage.setMinHeight(WINDOW_HEIGHT);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    // SETS ALL EVENT HANDLERS
    public void setHandlers() {
        baseFromField.setOnKeyPressed(e -> { processGenerate(e.getCode()); });
        baseToField.setOnKeyPressed(e -> { processGenerate(e.getCode()); });
        rangeField.setOnKeyPressed(e -> { processGenerate(e.getCode()); });
        generateButton.setOnAction(e -> { processGenerate(KeyCode.ENTER); });
        
        answerField.setOnKeyPressed(e -> { processCheck(e.getCode()); });
        checkButton.setOnAction(e -> { processCheck(KeyCode.ENTER); });
        
        answerButton.setOnAction(e -> { processToggleSolution(); });
        
        quitButton.setOnAction(e -> { System.exit(0); });
    }
    
    // SETS ALL PROPERTY LISTENERS
    public void setListeners() {
        baseFromField.textProperty().addListener(e -> { disableControls(); });
        baseToField.textProperty().addListener(e -> { disableControls(); });
        rangeField.textProperty().addListener(e -> { disableControls(); });
        
        answerField.textProperty().addListener(e -> { statusLabel.setText(""); });
    }
    
    // GENERATES A NEW RANDOM VALUE USING THE BASES OBJECT
    public boolean generateValue() {
        String baseFrom = baseFromField.getText();
        String baseTo   = baseToField.getText();
        String rangeStr = rangeField.getText();
        
        if(!isValidDigits(baseFrom) || !isValidDigits(baseTo) || !isValidDigits(rangeStr)) {
            dialog.show("Text Error", "Please only enter digits in the fields.");
            return false;
        }
        
        int startBase   = Integer.parseInt(baseFrom);
        int endBase     = Integer.parseInt(baseTo);
        int range       = Integer.parseInt(rangeStr);
        
        if(!bases.isValidBase(startBase) || !bases.isValidBase(endBase) ) {
            dialog.show("Base Error", "The base must be between 2 and 36.");
            return false;
        }
        
        if(range < 10) {
            dialog.show("Range Error", "The range must be at least 10.");
            return false;
        }
        
        bases.setStartBase(startBase);
        bases.setEndBase(endBase);
        bases.setRange(range);

        bases.generateRandomValue();
        
        return true;
    }
    
    public void processGenerate(KeyCode code) {
        if(code != KeyCode.ENTER) { return; }
        
        if(!generateValue()) { return; }
        
        promptLabel.setText("Base " + bases.getStartBase()
                + " --> Base " + bases.getEndBase());
        valueField.setText(bases.getStartValue());

        // ENABLE CONTROLS
        enableControls();

        statusLabel.setText("");
    }
    
    public void processToggleSolution() {
        String solution = "Answer: " + bases.getEndValue();
        
        if(statusLabel.getText().equals(solution)) {
            statusLabel.setText("");
        } else {
            statusLabel.setText(solution);
        }
    }
    
    public void processCheck(KeyCode code) {
        if(code != KeyCode.ENTER) { return; }
        
        try {
            String userAnswer = answerField.getText().toUpperCase();

            if(bases.getEndValue().equals(userAnswer)) {

                processGenerate(KeyCode.ENTER);
                statusLabel.setText("That was correct!");

            } else {
                statusLabel.setText("That was incorrect.");
            }
        } catch(Exception ex) {
            dialog.show("Error", "An invalid answer was given.");
            statusLabel.setText("Invalid answer.");
        }
    }
    
    public void enableControls() {
        answerField.setDisable(false);
        valueField.setDisable(false);
        answerButton.setDisable(false);
        checkButton.setDisable(false);
    }
    
    public void disableControls() {
        answerField.setDisable(true);
        valueField.setDisable(true);
        answerButton.setDisable(true);
        checkButton.setDisable(true);
        promptLabel.setText("");
        answerField.setText("");
        valueField.setText("");
        statusLabel.setText("");
    }
    
    // HELPER METHODS
    
    private HBox alignedHBox(Node left, Node right) {
        
        HBox leftHBox = new HBox(left);
        leftHBox.setAlignment(Pos.CENTER_LEFT);
        HBox.setHgrow(leftHBox, Priority.ALWAYS);
        
        HBox rightHBox = new HBox(right);
        rightHBox.setAlignment(Pos.CENTER_RIGHT);
        HBox.setHgrow(rightHBox, Priority.ALWAYS);
        
        return new HBox(leftHBox, rightHBox);
    }
    
    private HBox alignedHBox(Node left, Node middle, Node right) {
        HBox container = alignedHBox(left, right);
        
        HBox middleHBox = new HBox(middle);
        middleHBox.setAlignment(Pos.CENTER);
        HBox.setHgrow(middleHBox, Priority.ALWAYS);
        
        container.getChildren().add(1, middleHBox);
        
        return container;
    }
    
    private TextField buildTextField(String text, int width) {
        TextField field = new TextField(text);
        field.setPrefWidth(width);
        
        return field;
    }
    
    private boolean isValidDigits(String text) {
        if(text.isEmpty()) {
            return false;
        }
        
        for(int i = 0; i < text.length(); i++) {
            if((text.charAt(i) < '0' || text.charAt(i) > '9')
                    && text.charAt(i) != '-') {
                
                return false;
            }
        }
        
        return true;
    }
    
}
